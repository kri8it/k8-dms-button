## CHANGELOG

= 1.0 =
* Initial release 

= 1.0.1 =
* Added Github updater support

= 1.0.2 =
* K8 Updater compatibility

= 1.0.3 =
* Added some custom styles