<?php
/*
	Plugin Name: K8 DMS Button
	Plugin URI: http://www.kri8it.com
	Description: Add a button
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.3
	Section: true
	Class Name: K8Button
	Filter: component
	Loading: active

    K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-button
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;
	
class K8Button extends PageLinesSection {
	
	function section_persistent(){
        
		
		
    }
		
	function section_template() {
			
		$button_text 	= $this->opt( 'k8_button_text', array( 'default' => 'Click Here' ) );
		$button_icon 	= $this->opt( 'k8_button_icon', array( 'default' => '' ) );
		$button_size 	= $this->opt( 'k8_button_size', array( 'default' => 'default' ) );
		$button_color 	= $this->opt( 'k8_button_color', array( 'default' => 'btn-primary' ) );
		$button_link 	= $this->opt( 'k8_button_link', array( 'default' => '' ) );
		$button_href 	= $this->opt( 'k8_button_href', array( 'default' => '' ) );
		
		$target = ( $this->opt( 'k8_button_target_blank' ) ? 'target="_blank"' : '' );
		
		$href = ( $button_href != '' ? $button_href : $button_link );
		
		$button = '<a title="' . $button_text . '" ' .$target. ' href="' . $href . '" class="btn ' . $button_size . ' ' . $button_color . '">' . $button_text . '<i class="fa fa-' . $button_icon . '"></i></a>';
		
		echo $button;
		
	}

	function section_opts(){
		
		$pages = get_posts( 'post_type=page&posts_per_page=-1' );
		$posts = get_posts( 'post_type=post&posts_per_page=-1' );		
		$products = get_posts( 'post_type=product&posts_per_page=-1' );
		$team = get_posts( 'post_type=team&posts_per_page=-1' );
		$portfolio = get_posts( 'post_type=portfolio&posts_per_page=-1' );
		
		$links = array_merge( $pages, $posts, $products, $team, $portfolio );
		
		$link_array = array();
		foreach( $link_array as $link ):
			
			$link_array[$link->ID] = array( 'name' => $link->post_title ); 
			
		endforeach;
		
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_button_settings',
				'col'		=> 1,
				'opts'		=> array(
					array(
						'key'			=> 'k8_button_text',
						'type' 			=> 'text',
						'label' 		=> __( 'Button Text', 'pagelines' ),
					),
					array(
						'key'			=> 'k8_button_icon',
						'type' 			=> 'select_icon',
						'label' 		=> __( 'Button Icon (optional)', 'pagelines' ),
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_button_size',
						'label' 		=> __( 'Button Size', 'pagelines' ),
						'default'		=> 'default',
						'opts'			=> array(
							'btn-mini'			=> array('name' => 'Mini'),
							'btn-default'		=> array('name' => 'Default'),
							'btn-large'			=> array('name' => 'Large')
						)
					),
					array(
						'key'			=> 'k8_button_color',						
						'label'			=> __( 'Button Color', 'pagelines' ),
						'type'          => 'select_button', 						
					),
					array(
						'key'			=> 'k8_button_target_blank',						
						'label'			=> __( 'Open in new tab', 'pagelines' ),
						'type'			=> 'check',
						'default'		=> false
					),
					array(
						'type'          => 'select',		                
		                'key'           => 'k8_button_link',
		                'label'         => 'Select Link',
		                'opts'			=> $link_array
					),
					array(
						'key'			=> 'k8_button_href',
						'type' 			=> 'text',
						'label'			=> __( 'Custom URL', 'pagelines' ),
					),								
				)
			)
		);
		return $opts;		
	}
}